## web-todo
Simple browser-based todo app inspired by complice.co:

## Description
- Basic functionality
    - Keep track of TODOs (made up of goals, tasks and intentions)
    - Pomodoro timer
    - Sessions to track progress
- TODOs are structured in a goal > task > intention hierarchy
    - goals are largish projects that take days or longer to complete
    - tasks take multiple pomodoros
    - intentions are small tasks that can be completed within a session of O(h)
- Session usage
    - note down your intentions for the session
    - work towards them by completing pomodoros
    - assign each pomodoro to an intention
    - check off intentions as they are completed
    - after stopping a session you can add comments to each intention

## Installation / Usage
Download and open index.html in your browser.

## Roadmap
Feature TODO:
- unify session and checkpoint system
- use checkpoints for keeping track of (last) session status; save sessions for keeping track of goals and outcomes (almost DONE, some strange bug where localStorage is not persistent)
- use saved intentions to suggest new intentions in the next sessions
- visualize outcome statistics on demand
- incentivize priorization of closest deadline goal
- ask for goal/deadline update on after crossing deadline
- "Are you sure?" prompt/button feature before deleting intention/goal; alternative a global undo/redo button
- "This data is missing/wrong for your action" user feedback
- "Is this enough?" feedback in outcomes
- keep track of #pomos across sessions?-> design decision

- visual design to make goals more memorable
- memorable and useful audio
- use indexedDB instead of localStorage
- how to know if a browser has crashed? handle it
- figure out how controllers are usually used, i.e. one for each UI element?

Maintainance & Bugs:
- readability: add useful comments (partly done)
- structure and clean up code

finished:
- toggle button whether to play audio DONE
- bug where after break has ended you need to click the button again to reset DONE
- use $scope instead of todoList = this DONE
- add break timer DONE
- save sessions including current goals, intentions and outcomes DONE note: intentions are included in outcomes from an information standpoint; added possibility to store more metadata
- timer in hh:mm:ss format DONE
- subgoals with deadlines DONE
- display outcomes by goal DONE
- cannot delete while in session DONE
- cannot delete goals while corresponding intentions/outcomes are present DONE
- when deleting intentions after a session while outcomes are not saved a state of nothingness is reached DONE
- can add intention without specifying tag -> should default to generic DONE
- customizable timer with sound effect DONE
- can assign working sessions to intentions/tasks -> track work effort per tag DONE
- save intentions/outcomes log and feedback to degree of work satisfaction DONE
- possibility to tag intentions, i.e. assign them to longterm goals DONE
- write down intentions/tasks for the day DONE
- document the outcomes after/while working DONE
- clear everything after saving outcomes DONE
- (save) outcomes only clickable/visible after session DONE
- checkbox still works even though it is hidden DONE
- need to be able to remove intentions and goals DONE
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Project status
Development stopped
