// TODO implement indexedDB support
// if (!window.indexedDB) {
	// alert("IndexedDB is not supported by your browser! Please use a different browser if you want to carry over progress between sessions.");
// }
// const dbName = "TodoDB";
// const dbVersion = 2;
// const dbStore = "test";
// var request = window.indexedDB.open(dbName, dbVersion);

// request.onupgradeneeded = function(event) {
	// var db = event.target.result;
	// var objectStore = db.createObjectStore(dbStore, { keyPath: "id", autoIncrement: true });
	// objectStore.createIndex("datetime", "datetime", { unique: true });
// };

//$window.localStorage.clear(); // use this to reset your local Storage (maybe add interface element for this later)

// plays a beep sound (currently used to indicate the end of timers)
function playSound() {
	var sound = document.getElementById("audio");
	sound.play();
}

// the todo app object
var app = angular.module('todoApp', []);

// the controller
app.controller('TodoListController', function($scope, $interval, $window) {
	
	// date object at the time of starting this app
	var today = new Date(); 
	
	// setup session count in local storage
	if(!$window.localStorage.getItem("sessionCount")) {
		$window.localStorage.setItem("sessionCount", 0);
	}
	
	// sets suggestions based on a (previous) outcome list
	$scope.setTodoSugg = function(outcomes) {
		angular.forEach(outcomes, function(outcome) {
			if(!outcome.done) {
				$scope.todoSugg.push(outcome);
			}
		});
	};
	
	// loads a certain session if existent
	$scope.loadSession = function($sessionCount) {
		if($sessionCount === undefined) {
			// get the current session count
			sessionCount = Number($window.localStorage.getItem("sessionCount"));
		}
		// if available retrieve the last recorded session
		var sessionX;
		if(sessionCount > 0) {
			try {
				sessionX = JSON.parse($window.localStorage.getItem("Session" + (sessionCount - 1)));
			} catch(e) {
				console.log("Failed to load session "+sessionCount);
				return;
			}
		console.log("Loaded session ended on "+sessionX[0]);
		} else console.log("This is the very first session!");
		
		// if existent, load tags of previous session
		if(sessionX) {
			lastTags = sessionX[1];
			angular.forEach(lastTags, function(tag) {
				if(tag.value && tag.value != 0) {
					tag.used = false;
					$scope.tags.push(tag);
				}
			});

			lastOutcomes = sessionX[2];
			$scope.setTodoSugg(lastOutcomes);
			
		}
	};
	
	$scope.existsCheckpoint = function() {
		return $window.localStorage.getItem("checkpoint") !== null;
	};
	
	// the current todo list
	$scope.todos = [
	  //{text:'learn AngularJS', done:false, tag: 2, numPomos: 0},
	  //{text:'plan future & reduce uncertainty', done:false, tag: 1, numPomos: 0}
	  ];
	  
	// suggestions of todos from not completed tasks of last session
	$scope.todoSugg = [];
	
	// the current outcome list
	$scope.outcomes = [];
	
	// the current list of tags (the generic tag is present per default)
	$scope.tags = [{text:'generic', value: 0, used: false}//, 
	  //{text:'life is life', value: 1, goal: 'eliminate procrastination', deadline: today, used: true},
	  //{text:'studying', value: 2, goal: '', used: true}
	  ];
	
	// together isWorking and sessionSaved determine the state of the session
	$scope.isWorking = false;
	$scope.sessionSaved = true;
	// checkpoint system related variables
	$scope.lastCheckpointTime = undefined;
	$scope.existsCheckpointFlag = $scope.existsCheckpoint();
	// button text for session control
	$scope.workText = 'start session';
	// default tag setting is generic for adding new todo
	$scope.todoTag = '0';
	// default tag for setting a goal is unspecified (because one can't set a generic goal)
	$scope.goalTag;
	// default deadline is one week from now
	$scope.deadline = new Date(today.getTime() + 7 * 24 * 60 * 60 * 1000);
 
	// controls the state of the session
	$scope.toggleWork = function() {
		if($scope.isWorking && !$scope.sessionSaved) {
			$scope.endSession();
			$scope.workText = 'save session';
			$scope.isWorking = false;
		} else if(!$scope.isWorking && !$scope.sessionSaved){
			$scope.saveSession();
			$scope.workText = 'start session';
		} else if(!$scope.isWorking && $scope.sessionSaved){
			$scope.startSession();
			$scope.workText = 'stop session';
			$scope.isWorking = true;
			$scope.sessionSaved = false;
		}
		$scope.saveCheckpoint();
	};
 
	// adds a todo to next sessions list
	$scope.addTodo = function() {
	  if($scope.todoText && $scope.todoText != '') {
		  $scope.todos.push({text:$scope.todoText, done:false, tag: Number($scope.todoTag), numPomos: 0});
		  $scope.todoText = '';
		  $scope.updateTagUsage(Number($scope.todoTag));
	  }

	};
	
	// deletes a todo from the list
	$scope.deleteTodo = function(index) {
		remTodo = $scope.todos[index];
		$scope.todos.splice(index, 1);
		$scope.updateTagUsage(remTodo.tag);
		
		$scope.saveCheckpoint();
	};
	
	$scope.addSugg = function(index) {
		moveSugg = $scope.todoSugg[index];
		$scope.todoSugg.splice(index, 1);
		$scope.todos.push(moveSugg);
		$scope.updateTagUsage(moveSugg.tag);
		
		$scope.saveCheckpoint();
	};
	
	// sets a goal for a specified tag
	$scope.setGoal = function() {
		// TODO notify user about wrong/missing info
		console.log($scope.deadline > today)
		if($scope.goalText && $scope.goalText != '' && $scope.goalTag && $scope.deadline > today) {
			var tagInd = $scope.tags.findIndex(element => element.value == $scope.goalTag);
			$scope.tags[tagInd].goal = $scope.goalText;
			$scope.tags[tagInd].deadline = $scope.deadline;
			$scope.goalText = '';
		}
		
		$scope.saveCheckpoint();
	};
	
	// adds a tag
	$scope.addTag = function() {
	  if($scope.tagText && $scope.tagText != '') {
		  $scope.tags.push({text:$scope.tagText, value:$scope.tags.length});
		  $scope.tagText = '';
	  }
	  
	  $scope.saveCheckpoint();
	};
	
	// deletes a tag
	$scope.deleteTag = function(index) {
		$scope.tags.splice(index, 1);
	
		$scope.saveCheckpoint();
	};
	
	// updates the 'used' attribute of a tag
	$scope.updateTagUsage = function(checktag) {
		indCheck = $scope.tags.findIndex(element => element.value == checktag);
		var used = false;
		angular.forEach($scope.todos, function(todo) {
			if(todo.tag == checktag){
				used = true;
			}
		});
		angular.forEach($scope.outcomes, function(outcome) {
			if(outcome.tag == checktag){
				used = true;
			}
		});
		$scope.tags[indCheck].used = used;
	};
	
	// counts the remaining todos
	$scope.remaining = function() {
	  var count = 0;
	  angular.forEach($scope.todos, function(todo) {
		count += todo.done ? 0 : 1;
	  });
	  return count;
	};
 
	// (not  used) should be removed soon
	$scope.archive = function() {
	  var oldTodos = $scope.todos;
	  $scope.todos = [];
	  angular.forEach(oldTodos, function(todo) {
		if (!todo.done){
			$scope.todos.push(todo);
		} else {
			todo.comment = ''
			$scope.outcomes.push(todo)
		}
	  });
	};
	
	// do things needed to start a session; currently nothing todo due to visibility bindings etc. (maybe change control variables here in the future)
	$scope.startSession = function() {
		/*var allIntentionsString = JSON.stringify($scope.todos);
		var now = (new Date()).toLocaleString();
		var intentionId = Number($window.localStorage.getItem("intentionCount"));
		$window.localStorage.setItem("intentionCount", intentionId + 1);
		$window.localStorage.setItem("Int" + String(intentionId), now);
		$window.localStorage.setItem(now, allIntentionsString);
		console.log("Int" + String(intentionId));
		console.log("saved intentions");*/
		$scope.todoSugg = [];
	};
	
	// prepare to save the session
	$scope.endSession = function() {
		var oldTodos = $scope.todos;
		angular.forEach(oldTodos, function(todo) {
			console.log(todo);
			todo.comment = ''
			$scope.outcomes.push(todo);
		});
	};
	
	// saves the current state of the program in a checkpoint
	$scope.saveCheckpoint = function() {
		var now = (new Date()).toLocaleString();
		var stateVars = [$scope.isWorking, $scope.sessionSaved, $scope.pomoState];
		var checkpointString = angular.toJson([now, stateVars, $scope.tags, $scope.todos, $scope.outcomes]);
		$window.localStorage.setItem("checkpoint", checkpointString);
		
		$scope.lastCheckpointTime = now;
		$scope.existsCheckpointFlag = $scope.existsCheckpoint();
	};
	
	
	// retrieves and configures program according to the last checkpoint
	$scope.loadCheckpoint = function() {
		if(!$scope.existsCheckpoint()) return;
		try {
			checkpointJSON = $window.localStorage.getItem("checkpoint");
			checkpoint = JSON.parse(checkpointJSON);
		} catch(e) {
			console.log("Failed to load checkpoint.")
			return;
		}

		var now = checkpoint[0]
		var stateVars = checkpoint[1]
		var chTags = checkpoint[2]
		var chTodos = checkpoint[3]
		var chOutcomes = checkpoint[4]
		$scope.isWorking = stateVars[0]
		$scope.sessionSaved = stateVars[1]
		$scope.pomoState = stateVars[2]
		if($scope.isWorking && !$scope.sessionSaved) $scope.workText = 'stop session';
		else if(!$scope.isWorking && !$scope.sessionSaved) $scope.workText = 'save session';
		else if(!$scope.isWorking && $scope.sessionSaved) $scope.workText = 'start session';
		if($scope.pomoState == 1) $scope.timerText = 'cancel pomo';
		else if($scope.pomoState == 2) $scope.timerText = 'assign pomo';
		else if($scope.pomoState == 3) $scope.timerText = 'start pomo';
		
		$scope.tags = chTags;
		$scope.todos = chTodos;
		$scope.outcomes = chOutcomes;
		
		$scope.lastCheckpointTime = now;
	};

	$scope.clearCheckpoint = function() {
		console.log($window.localStorage.getItem("checkpoint"));
		$window.localStorage.setItem("checkpoint", null);
		console.log($window.localStorage.getItem("checkpoint"));
		$scope.existsCheckpointFlag = $scope.existsCheckpoint();
	};
	
	// saves the finished session to the local storage
	$scope.saveSession = function() {
		// prepare session data
		var now = (new Date()).toLocaleString();
		var sessionString = JSON.stringify([now, $scope.tags, $scope.outcomes]);
		// get session count
		var sessionCount = Number($window.localStorage.getItem("sessionCount"));
		// update session count
		$window.localStorage.setItem("sessionCount", sessionCount + 1);
		// save session ID with session string
		$window.localStorage.setItem("Session" + String(sessionCount), sessionString);
		// set 'saved' state
		$scope.sessionSaved = true;
		console.log("saved outcomes");
		// set unfinished outcomes as suggestions for next session
		$scope.setTodoSugg($scope.outcomes);
		// clear intentions and outcomes
		$scope.todos = [];
		$scope.outcomes = [];
		// update 'used' attribute of each tag
		for(i = 0; i < $scope.tags.length; i++) {
			$scope.updateTagUsage($scope.tags[i].value);
		}
		// remove last checkpoint of this session
		$scope.clearCheckpoint();
	};
	
	// general timer variables
	$scope.countDownText = new Date(0).toISOString().substr(11, 8)
	var stop;
	$scope.audioFlag = true;
	
	// variables of the break timer
	$scope.breakText = 'start break';
	$scope.breakRunning = false; // state variable of break  timer
	$scope.breakTime = 5; // in minutes
	
	// controls the break buttons' functionality
	$scope.runBreak = function() {
		if($scope.breakRunning) {
			$scope.stopTimerBreak()
			$scope.breakRunning = false;
		} else {
			$scope.startTimer($scope.breakTime*60, false);
			$scope.breakRunning = true;
		}
	};
	
	// variables of the pomodoro timer

	$scope.pomoState = 0; // state variable to distinguish between (0: not running,  1: running, 2: not assigned)
	$scope.pomoTime = 25; // in minutes
	$scope.pomoAssign; // the todo to assign a finished pomodoro to
	
	// setup timer text
	$scope.timerText = 'start pomo';
	
	// controls the pomodoro timer
	$scope.togglePomo = function() {
		if($scope.pomoState == 1) {
			$scope.stopTimerPomo();
		} else if($scope.pomoState == 0) {
			$scope.startTimer($scope.pomoTime*60, true);
			$scope.pomoState = 1;
			$scope.timerText = 'cancel pomo';
		} else if($scope.pomoState == 2) {
			if(angular.isDefined($scope.pomoAssign)) {
				$scope.todos[$scope.pomoAssign].numPomos += 1;
				$scope.pomoState = 0;
				$scope.timerText = 'start pomo'
				
				$scope.saveCheckpoint();
			}
		}
	};
	
	// starts a timer of 'timesecs' seconds and displays the time left; runs different stop functions for both timers depending on the 'pomo' flag 
	$scope.startTimer = function(timesecs, pomo) {
		$scope.countDown = timesecs;
		stop = $interval(function(){
			if($scope.countDown > 0) {
				--$scope.countDown
				$scope.countDownText = new Date($scope.countDown*1000).toISOString().substr(11, 8)
			} else {
				if(pomo) $scope.stopTimerPomo();
				else $scope.stopTimerBreak();
			}}, 1000);
	};
	
	// stops the break timer
	$scope.stopTimerBreak = function() {
		if(angular.isDefined(stop)) {
            $interval.cancel(stop);
            stop = undefined;
        }
		$scope.countDownText = new Date(0).toISOString().substr(11, 8)
		if($scope.countDown == 0 && $scope.audioFlag) {
			playSound();
		}
		$scope.breakRunning = false;
	};
	
	// stops the pomodoro timer
	$scope.stopTimerPomo = function() {
		if(angular.isDefined(stop)) {
            $interval.cancel(stop);
            stop = undefined;
        }
		if($scope.countDown == 0) {
			if($scope.audioFlag) playSound();
			$scope.timerText = 'assign pomo';
			$scope.pomoState = 2;
		} else {
			$scope.timerText = 'start pomo';
			$scope.pomoState = 0;
		}
		$scope.countDownText = new Date(0).toISOString().substr(11, 8)
	};
	
	// makes sure timers started with $interval are stopped
	$scope.$on('$destroy', function() {
		if(angular.isDefined(stop)) {
            $interval.cancel(stop);
            stop = undefined;
        }
	});
	
	$scope.loadSession(undefined);
	$scope.loadCheckpoint();
});

